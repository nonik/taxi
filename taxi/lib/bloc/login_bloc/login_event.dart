part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginButtonPressed extends LoginEvent {
  final String phone;

  const LoginButtonPressed({
    required this.phone,
  });

  @override
  List<Object> get props => [phone];

  @override
  String toString() =>
      'LoginButtonPressed { phone: $phone }';
}
