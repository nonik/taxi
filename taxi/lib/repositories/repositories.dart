// ignore: import_of_legacy_library_into_null_safe
import 'package:dio/dio.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:taxi/services/user_api_provider.dart';

class MainRepository {
  static String mainUrl = "http://taxi/api";
  var loginUrl = '$mainUrl/login';
  var codeUrl = '$mainUrl/code';
  final FlutterSecureStorage storage = const FlutterSecureStorage();
  final Dio _dio = Dio();
}

class UserRepository extends MainRepository {
  final UserProvider _userProvider = UserProvider();

  Future<bool> hasToken() async {
    var value = await storage.read(key: 'access');
    return value != null;
  }

  Future<void> persistToken(String token) async {
    await storage.write(key: 'token', value: token);
  }

  Future<void> deleteToken() async {
    storage.delete(key: 'token');
    storage.deleteAll();
  }

  Future<String> getCode(String phone) async {
    print('------------------getCode phone: $phone');
    print('------------------getCode codeUrl: $codeUrl');
    Response response = await _dio.post(codeUrl, data: {
      "phone": phone,
    });
    print('------------------getCode response: $response');
    return response.data["code"];
  }

  Future<String> login(String phone, String code) async {
    Response response = await _dio.post(loginUrl, data: {
      "phone": phone,
      "verify_code": code,
    });
    return response.data["access"];
  }

  Future<String> getToken() async {
    if (await storage.containsKey(key: 'token')) {
      var token = await storage.read(key: 'token');

      if (token != null && token != '') {
        return token;
      } else {
        throw Exception('Smth going wrong');
      }
    } else {
      throw Exception('Smth going wrong');
    }
  }

  Future<dynamic> getUser() async {
    var token = await getToken();
    return await _userProvider.getUser(token);
  }
}