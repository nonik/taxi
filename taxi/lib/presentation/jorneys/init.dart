import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxi/main_slider.dart';
import 'package:taxi/presentation/jorneys/login_screen.dart';
import 'package:taxi/presentation/themes/theme.dart' as style;
import 'package:taxi/presentation/widgets/side_drawer.dart';
import 'package:taxi/repositories/repositories.dart';

class InitApp extends StatefulWidget {
  const InitApp({Key? key}) : super(key: key);

  @override
  InitState createState() => InitState();
}

class InitState extends State<InitApp> {
  final userRepository = UserRepository();
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(scaffoldBackgroundColor: style.Colors.background),
      home: Builder(builder: (context) => Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: const Text(
            'Главная',
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme:
              const IconThemeData(color: Colors.black, size: 40),
        ),
        drawer: SideDrawer(),
        body: ElevatedButton(
          child: Text('data'),
          onPressed: () { 
            Navigator.push(context,
              CupertinoPageRoute(builder: (_) => LoginScreen(userRepository: userRepository)));
           },
        ))),
    );
  }
}
