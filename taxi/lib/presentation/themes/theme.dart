import 'dart:ui';

import 'package:flutter/cupertino.dart';

class Colors {
  const Colors();

  static const Color mainColor = Color.fromRGBO(88, 186, 171, 1);
  static const Color textMain = Color.fromRGBO(1, 1, 1, 1);
  static const Color background = Color.fromRGBO(255, 255, 255, 1);
  static const Color grey = Color(0xFFb4bdce);

  static const primaryGradient = LinearGradient(
    colors: [Color(0xFF5BC0FF), Color(0xFF0063FF)],
    stops: [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}
