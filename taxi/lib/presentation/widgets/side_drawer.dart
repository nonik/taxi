import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxi/bloc/user_bloc/user_bloc.dart';
import 'package:taxi/bloc/user_bloc/user_event.dart';
import 'package:taxi/presentation/jorneys/authorization.dart';
import 'package:taxi/repositories/repositories.dart';

class SideDrawer extends StatefulWidget {
  const SideDrawer({Key? key}) : super(key: key);

  @override
  _SideDrawerState createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {
  final userRepository = UserRepository();

  @override
  Widget build(BuildContext context) {
    var padding = MediaQuery.of(context).padding;

    return BlocProvider<UserBloc>(
      create: (context) =>
          UserBloc(userRepository: userRepository)..add(UserLoadEvent()),
      child: Drawer(
          child: ListView(
            children: const <Widget>[
              ListTile(
                leading: Icon(Icons.account_circle_rounded, size: 45,),
                title: Text("Елдос Сайлауов"),
                subtitle: Text("+7 708 709 01 30"),
                // trailing: Icon(Icons.arrow_back),
                // onTap: (){}
              ),
              ListTile(
                // leading: Icon(Icons.settings),
                title: Text("Главная"),
                // trailing: Icon(Icons.arrow_downward),
                // onTap: (){}
              ),
              ListTile(
                // leading: Icon(Icons.settings),
                title: Text("Уведомление"),
                // trailing: Icon(Icons.arrow_downward),
                // onTap: (){}
              ),
              ListTile(
                // leading: Icon(Icons.settings),
                title: Text("Межгород"),
                // trailing: Icon(Icons.arrow_downward),
                // onTap: (){}
              ),
              ListTile(
                // leading: Icon(Icons.settings),
                title: Text("Служба поддержки"),
                // trailing: Icon(Icons.arrow_downward),
                // onTap: (){}
              ),
            ],
          ),
        ),
    );
  }
}
