class Provider {
  static String mainUrl = "http://taxi/api";
  var userUrl = '$mainUrl/v1/auth/user';

  dynamic filterVisitUrl(
    selectedFromDate,
    selectedToDate,
  ) =>
      '$mainUrl/v1/visits?page_size=9999999&auth=1&from_date=$selectedFromDate&to_date=$selectedToDate';
}
