import 'package:dio/dio.dart';
import 'package:taxi/models/user.dart';
import 'package:taxi/services/api_provider.dart';

class UserProvider extends Provider {
  Future<dynamic> getUser(token) async {
    final Dio _dio = Dio();
    _dio.options.headers['Accept'] = 'application/json';
    _dio.options.headers["Authorization"] = "Bearer $token";

    Response response = await _dio.get(userUrl);

    if (response.statusCode == 200) {
      Map<String, dynamic> userJson = Map<String, dynamic>.from(response.data);
      return User.fromJson(userJson);
    } else {
      throw Exception('Error user fetching');
    }
  }
}
